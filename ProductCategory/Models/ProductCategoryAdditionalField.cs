﻿namespace ProductCategory.Models
{
    public class ProductCategoryAdditionalField
    {
        public int Id { get; set; }
        public required string Name { get; set; }
        public AdditionalFieldType Type { get; set; }
        public int ProductCategoryId { get; set; }
        public ProductCategory ProductCategory { get; set; } = null!;
        public ICollection<ProductCategoryAdditionalFieldValue> Values { get; } = new List<ProductCategoryAdditionalFieldValue>();
    }
}
