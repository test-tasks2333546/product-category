﻿namespace ProductCategory.Models
{
    public class ProductCategory
    {
        public int Id { get; set; }
        public required string Name { get; set; }
        public ICollection<Product> Products { get; } = new List<Product>();
        public ICollection<ProductCategoryAdditionalField> AdditionalFields { get; } = new List<ProductCategoryAdditionalField>();
    }
}
