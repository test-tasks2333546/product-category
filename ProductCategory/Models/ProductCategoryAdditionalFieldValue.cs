﻿namespace ProductCategory.Models
{
    public class ProductCategoryAdditionalFieldValue
    {
        public int Id { get; set; }
        public string? Value { get; set; }
        public int FieldId { get; set; }
        public ProductCategoryAdditionalField Field { get; set; } = null!;
        public int ProductId { get; set; }
        public Product Product { get; set; } = null!;
    }
}
