﻿namespace ProductCategory.Models
{
    public struct FilterOperator
    {
        /// <summary>
        /// Equals.
        /// </summary>
        public const string EQ = "eq";
        /// <summary>
        /// Greater than.
        /// </summary>
        public const string GT = "gt";
    }
}
