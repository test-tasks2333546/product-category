﻿namespace ProductCategory.Models
{
    public enum AdditionalFieldType
    {
        String,
        Integer
    }
}
