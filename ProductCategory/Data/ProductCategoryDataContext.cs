﻿using Microsoft.EntityFrameworkCore;
using ProductCategory.Models;

namespace ProductCategory.Data
{
    public class ProductCategoryDataContext : DbContext
    {
        public ProductCategoryDataContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Models.ProductCategory> ProductCategories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategoryAdditionalField> ProductCategoryAdditionalFields { get; set; }
        public DbSet<ProductCategoryAdditionalFieldValue> ProductCategoryAdditionalFieldValues { get; set; }
    }
}
