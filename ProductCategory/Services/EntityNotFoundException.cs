﻿using System.Runtime.Serialization;


namespace ProductCategory.Services
{
    [Serializable]
    public class EntityNotFoundException : Exception
    {
        public string Name { get; }
        public object Key { get; }

        public EntityNotFoundException(string name, object key) : base()
        {
            Name = name;
            Key = key;
        }

        protected EntityNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Name = (string)info.GetValue(nameof(Name), typeof(string))!;
            Key = info.GetValue(nameof(Key), typeof(object))!;
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(nameof(Name), Name);
            info.AddValue(nameof(Key), Key);
            base.GetObjectData(info, context);
        }
    }
}
