﻿using ProductCategory.Models;

namespace ProductCategory.Services
{
    public static class AdditionalFieldTypeValidator
    {
        public static bool Validate(string? value, AdditionalFieldType targetType)
        {
            return targetType switch
            {
                AdditionalFieldType.String => true,
                AdditionalFieldType.Integer => int.TryParse(value, out _),
                _ => throw new ArgumentException("Unknown target type."),
            };
        }
    }
}
