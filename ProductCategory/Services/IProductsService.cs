﻿using ProductCategory.Dto.Product;
using ProductCategory.Dto.ProductCategory;

namespace ProductCategory.Services
{
    public interface IProductsService
    {
        public Task<int> Create(CreateProduct dto);
        public Task<ICollection<ProductWithAdditionalFields>> FindAll(ProductCategoryFilter filter);
        public Task<ProductWithAdditionalFields?> Get(int id);
    }
}
