﻿using ProductCategory.Dto.ProductCategory;

namespace ProductCategory.Services
{
    public interface IProductCategoriesService
    {
        public Task<int> Create(CreateProductCategory dto);
        public Task<ICollection<ProductCategoryWithAdditionalFields>> FindAll();
        public Task<bool?> Delete(int id);
    }
}
