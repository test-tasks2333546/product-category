﻿using Microsoft.EntityFrameworkCore;
using ProductCategory.Data;
using ProductCategory.Dto.Product;
using ProductCategory.Dto.ProductCategory;
using ProductCategory.Models;

namespace ProductCategory.Services
{
    public class ProductsService : IProductsService
    {
        private readonly ProductCategoryDataContext _dbContext;

        public ProductsService(ProductCategoryDataContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<int> Create(CreateProduct dto)
        {
            var newProduct = new Product() { Description = dto.Description, Price = dto.Price, Name = dto.Name };
            _dbContext.Products.Add(newProduct);

            /* Category (optional) */
            if (dto.CategoryId is not null)
            {
                var category = await _dbContext.ProductCategories.FindAsync(dto.CategoryId);
                if (category == null)
                {
                    throw new EntityNotFoundException(nameof(Models.ProductCategory), dto.CategoryId);
                }
                newProduct.Category = category;
            }

            /* Additional fields */
            var additionalFieldsIds = dto.AdditionalFields.Select(x => x.Id).ToArray();
            var fields = await _dbContext.ProductCategoryAdditionalFields.Where(x =>
                additionalFieldsIds.Contains(x.Id)
            ).ToListAsync();

            foreach (var additionalField in dto.AdditionalFields)
            {
                var field = fields.Find(x => x.Id == additionalField.Id);
                if (field == null)
                {
                    throw new EntityNotFoundException(nameof(Models.ProductCategoryAdditionalField), additionalField.Id);
                }
                if (!AdditionalFieldTypeValidator.Validate(additionalField.Value, field.Type))
                {
                    throw new Exception(string.Format("Invalid type for additional field; Id: {0}", additionalField.Id));
                }

                _dbContext.ProductCategoryAdditionalFieldValues.Add(
                    new Models.ProductCategoryAdditionalFieldValue() { Field = field, Value = additionalField.Value, Product = newProduct }
                );
            }

            _dbContext.Products.Add(newProduct);
            await _dbContext.SaveChangesAsync();

            return newProduct.Id;
        }

        public async Task<ICollection<ProductWithAdditionalFields>> FindAll(ProductCategoryFilter filter)
        {
            /* Validate filter */
            var additionalFieldsIds = filter.AdditionalFields.Select(x => x.Id).ToArray();
            var fields = await _dbContext.ProductCategoryAdditionalFields.Where(x =>
                additionalFieldsIds.Contains(x.Id)
            ).ToListAsync();

            foreach (var additionalFieldFilter in filter.AdditionalFields)
            {
                var field = fields.Find(x => x.Id == additionalFieldFilter.Id)
                    ?? throw new EntityNotFoundException(nameof(Models.ProductCategoryAdditionalField), additionalFieldFilter.Id);
                if (!AdditionalFieldTypeValidator.Validate(additionalFieldFilter.Value, field.Type))
                {
                    throw new Exception(string.Format("Invalid type for additional field; Id: {0}", additionalFieldFilter.Id));
                }
                if (!FilterOperatorValidator.Validate(additionalFieldFilter.Operator, field.Type))
                {
                    throw new Exception(string.Format("Operator `{0}` not supported for type {1}", additionalFieldFilter.Operator, field.Type));
                }
            }

            /* Apply filter (db) */
            var products = await _dbContext.Products
                .Include(x => x.Category)
                .Include(x => x.AdditionalFieldValues)
                .ThenInclude(x => x.Field)
                .Where(x => x.CategoryId == filter.CategoryId)
                .ToListAsync();

            /* Apply filter (c#) */
            products = products.Where(x =>
                filter.AdditionalFields.All(f =>
                    x.AdditionalFieldValues.Any(a =>
                        a.FieldId == f.Id
                        && FilterAdditionalField(f, a)
            ))).ToList();

            /* Map to Dto */
            return products.Select(x => new ProductWithAdditionalFields()
            {
                Name = x.Name,
                Description = x.Description,
                Id = x.Id,
                Price = x.Price,
                Category = new ProductCategoryDto() { Id = x.Category!.Id, Name = x.Category.Name },
                AdditionalFields = x.AdditionalFieldValues.Select(a =>
                    new ProductCategoryAdditionalFieldWithValue()
                    {
                        Id = a.Id,
                        Name = a.Field.Name,
                        Type = a.Field.Type,
                        Value = a.Value
                    }).ToList()
            }).ToList();
        }

        private bool FilterAdditionalField(
            ProductCategoryAdditionalFieldFilter filter, Models.ProductCategoryAdditionalFieldValue fieldValue)
        {
            switch (fieldValue.Field.Type)
            {
                case AdditionalFieldType.String:
                    return filter.Operator switch
                    {
                        FilterOperator.EQ => fieldValue.Value == filter.Value,
                        _ => throw new ArgumentException(),
                    };
                case AdditionalFieldType.Integer:
                    switch (filter.Operator)
                    {
                        case FilterOperator.EQ:
                            return fieldValue.Value == filter.Value;
                        case FilterOperator.GT:
                            if (fieldValue.Value is null || filter.Value is null)
                            {
                                return false;
                            }
                            return int.Parse(fieldValue.Value) > int.Parse(filter.Value);
                        default:
                            throw new ArgumentException();
                    };
                default:
                    throw new ArgumentException();
            }
        }

        public Task<ProductWithAdditionalFields?> Get(int id)
        {
            throw new NotImplementedException();
        }
    }
}
