﻿using ProductCategory.Models;

namespace ProductCategory.Services
{
    public static class FilterOperatorValidator
    {
        private static readonly string[] _allowedIntegerOperators = new[] { FilterOperator.EQ, FilterOperator.GT };
        public static bool Validate(string op, AdditionalFieldType targetType)
        {
            return targetType switch
            {
                AdditionalFieldType.String => op == FilterOperator.EQ,
                AdditionalFieldType.Integer => _allowedIntegerOperators.Contains(op),
                _ => throw new ArgumentException("Unknown target type."),
            };
        }
    }
}
