using Microsoft.AspNetCore.Mvc;
using ProductCategory.Dto.ProductCategory;
using ProductCategory.Services;

namespace ProductCategory.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductCategoriesController : ControllerBase
    {
        private readonly IProductCategoriesService _productCategoriesService;

        public ProductCategoriesController(IProductCategoriesService productCategoriesService)
        {
            _productCategoriesService = productCategoriesService;
        }

        [HttpPost]
        public async Task<ActionResult<int>> Create(CreateProductCategory dto)
        {
            try
            {
                var task = _productCategoriesService.Create(dto);
                await task;
                return Ok(task.Result);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<ProductCategoryWithAdditionalFields>>> FindAll()
        {
            try
            {
                var task = _productCategoriesService.FindAll();
                await task;
                return Ok(task.Result);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete([FromRoute] int id)
        {
            try
            {
                var task = _productCategoriesService.Delete(id);
                await task;
                if (task.Result is null)
                {
                    return NotFound();
                }
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
