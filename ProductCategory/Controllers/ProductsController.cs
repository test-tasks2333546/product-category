using Microsoft.AspNetCore.Mvc;
using ProductCategory.Dto.Product;
using ProductCategory.Dto.ProductCategory;
using ProductCategory.Services;

namespace ProductCategory.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductsService _productsService;

        public ProductsController(IProductsService productsService)
        {
            _productsService = productsService;
        }

        [HttpPost]
        public async Task<ActionResult<int>> Create([FromBody] CreateProduct dto)
        {
            try
            {
                var task = _productsService.Create(dto);
                await task;
                return Ok(task.Result);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost("FilterAll")]
        public async Task<ActionResult<ICollection<ProductWithAdditionalFields>>> FindAll([FromBody] ProductCategoryFilter filter)
        {
            try
            {
                var task = _productsService.FindAll(filter);
                await task;
                return Ok(task.Result);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<ProductWithAdditionalFields>> Get([FromRoute] int id)
        {
            try
            {
                var task = _productsService.Get(id);
                await task;
                if (task.Result is null)
                {
                    return NotFound();
                }
                return Ok(task.Result);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
