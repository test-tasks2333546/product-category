﻿namespace ProductCategory.Dto.ProductCategory
{
    public record ProductCategoryFilter
    {
        public int CategoryId { get; set; }
        public ICollection<ProductCategoryAdditionalFieldFilter> AdditionalFields { get; set; } = new List<ProductCategoryAdditionalFieldFilter>();
    }
}
