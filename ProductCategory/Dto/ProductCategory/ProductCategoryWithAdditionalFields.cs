namespace ProductCategory.Dto.ProductCategory
{
    public record ProductCategoryWithAdditionalFields : ProductCategoryDto
    {
        public ICollection<ProductCategoryAdditionalField> AdditionalFields { get; set; } =  new List<ProductCategoryAdditionalField>();
    }
}
