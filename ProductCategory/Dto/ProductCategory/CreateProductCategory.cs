﻿namespace ProductCategory.Dto.ProductCategory
{
    public record CreateProductCategory : ProductCategoryBase
    {
        public ICollection<ProductCategoryAdditionalFieldBase> AdditionalFields { get; set; } = new List<ProductCategoryAdditionalFieldBase>();
    }
}
