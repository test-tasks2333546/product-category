﻿namespace ProductCategory.Dto.ProductCategory
{
    public record ProductCategoryAdditionalField : ProductCategoryAdditionalFieldBase
    {
        public int Id { get; set; }
    }
}
