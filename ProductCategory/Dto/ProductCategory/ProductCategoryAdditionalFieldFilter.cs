﻿using ProductCategory.Models;

namespace ProductCategory.Dto.ProductCategory
{
    public record ProductCategoryAdditionalFieldFilter
    {
        public int Id { get; set; }
        public string? Value { get; set; }
        public string Operator { get; set; } = FilterOperator.EQ;
    }
}