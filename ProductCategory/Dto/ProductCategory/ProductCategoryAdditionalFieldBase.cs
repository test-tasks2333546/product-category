﻿using ProductCategory.Models;

namespace ProductCategory.Dto.ProductCategory
{
    public record ProductCategoryAdditionalFieldBase
    {
        public required string Name { get; set; }
        public AdditionalFieldType Type { get; set; }
    }
}
