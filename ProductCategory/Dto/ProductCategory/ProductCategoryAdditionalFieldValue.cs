﻿namespace ProductCategory.Dto.ProductCategory
{
    public record ProductCategoryAdditionalFieldValue
    {
        public int Id { get; set; }
        public string? Value { get; set; }
    }
}
