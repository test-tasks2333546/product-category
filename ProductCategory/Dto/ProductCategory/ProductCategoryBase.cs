﻿namespace ProductCategory.Dto.ProductCategory
{
    public record ProductCategoryBase
    {
        public required string Name { get; set; }
    }
}
