namespace ProductCategory.Dto.ProductCategory
{
    public record ProductCategoryDto : ProductCategoryBase
    {
        public int Id { get; set; }
    }
}
