﻿namespace ProductCategory.Dto.ProductCategory
{
    public record ProductCategoryAdditionalFieldWithValue : ProductCategoryAdditionalField
    {
        public string? Value { get; set; }
    }
}
