﻿using ProductCategory.Dto.ProductCategory;

namespace ProductCategory.Dto.Product
{
    public record CreateProduct : ProductBase
    {
        public int? CategoryId { get; set; }
        public ICollection<ProductCategoryAdditionalFieldValue> AdditionalFields { get; set; } = new List<ProductCategoryAdditionalFieldValue>();
    }
}
