using ProductCategory.Dto.ProductCategory;

namespace ProductCategory.Dto.Product
{
    public record ProductWithAdditionalFields : ProductBase
    {
        public int Id { get; set; }
        public ProductCategoryDto? Category { get; set; }
        public ICollection<ProductCategoryAdditionalFieldWithValue> AdditionalFields { get; set; } = new List<ProductCategoryAdditionalFieldWithValue>();
    }
}
