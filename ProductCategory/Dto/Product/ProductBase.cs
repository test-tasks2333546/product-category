﻿namespace ProductCategory.Dto.Product
{
    public record ProductBase
    {
        public required string Name { get; set; }
        public string? Description { get; set; }
        public decimal Price { get; set; }
    }
}
