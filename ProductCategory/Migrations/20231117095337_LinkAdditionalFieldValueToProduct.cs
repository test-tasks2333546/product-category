﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProductCategory.Migrations
{
    /// <inheritdoc />
    public partial class LinkAdditionalFieldValueToProduct : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProductId",
                table: "ProductCategoryAdditionalFieldValues",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ProductCategoryAdditionalFieldValues_ProductId",
                table: "ProductCategoryAdditionalFieldValues",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductCategoryAdditionalFieldValues_Products_ProductId",
                table: "ProductCategoryAdditionalFieldValues",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductCategoryAdditionalFieldValues_Products_ProductId",
                table: "ProductCategoryAdditionalFieldValues");

            migrationBuilder.DropIndex(
                name: "IX_ProductCategoryAdditionalFieldValues_ProductId",
                table: "ProductCategoryAdditionalFieldValues");

            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "ProductCategoryAdditionalFieldValues");
        }
    }
}
